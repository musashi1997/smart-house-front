import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_course/config/data_manager.dart';
import 'package:flutter_course/config/endpoints.dart';
import 'package:flutter_course/models/light_state.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

enum AvailableSwitch {first,second,cooler}

class SwitchLightsButtons extends StatefulWidget {
  const SwitchLightsButtons({Key key}) : super(key: key);

  @override
  State<SwitchLightsButtons> createState() => _SwitchLightsButtons();
}

/// This is the private State class that goes with MyStatefulWidget.
class _SwitchLightsButtons extends State<SwitchLightsButtons> {
  bool _lights = false;
  bool _lights2 = false;
  bool _cooler = false;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder<LightsState>(
        future: API.getLightsState(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _lights = snapshot.data.l1state;
            _lights2 = snapshot.data.l2state;
            _cooler = snapshot.data.l3state;
            return Column(
              children: [
                SwitchListTile(
                  title: const Text('Primary Lights'),
                  subtitle: const Text('turns primary lights On/Off'),
                  value: _lights,
                  onChanged: (bool value) {
                    changeLightState(value: value, item: AvailableSwitch.first, context: context);
                  },
                  secondary: const Icon(Icons.lightbulb_outline),
                ),
                SwitchListTile(
                  title: const Text('Secondary Lights'),
                  subtitle: const Text('turns primary lights On/Off'),
                  value: _lights2,
                  onChanged: (bool value) {
                    changeLightState(value: value, item: AvailableSwitch.second, context: context);
                  },
                  secondary: const Icon(Icons.lightbulb_outline),
                ),
                SwitchListTile(
                  title: const Text('Cooler'),
                  subtitle: const Text('turns cooler control  On/Off'),
                  value: _cooler,
                  onChanged: (bool value) {
                    changeLightState(value: value, item: AvailableSwitch.cooler, context: context);
                  },
                  secondary: const Icon(Icons.air),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const CircularProgressIndicator();
        },
      ),
    );
  }

  Future<void> changeLightState({bool value, AvailableSwitch item, BuildContext context}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int userId = prefs.getInt('userId');
    final String token = prefs.getString('token');
    final id = item.index + 1;
    final response = await http.post(
      Uri.parse(LightEndpoints.lights),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'bearer $token',
      },
      body: jsonEncode(<String, dynamic>{'userId': userId, "L${id}state": value}),
    );

    if (response.statusCode == 200) {
      setState(
        () {
          switch (item){
            case AvailableSwitch.first:
              _lights = value;
              break;
            case AvailableSwitch.second:
              _lights2 = value;
              break;
            case AvailableSwitch.cooler:
              _cooler = value;
              break;
          }
        },
      );
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          backgroundColor: Colors.grey[900],
          content: const Text(
            'successfully changed light state',
            style: TextStyle(color: Colors.green),
          ),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          backgroundColor: Colors.grey[900],
          content: const Text(
            'something went wrong',
            style: TextStyle(color: Colors.red),
          ),
        ),
      );
      throw Exception('Failed to change LightState$id.');
    }
  }
}
