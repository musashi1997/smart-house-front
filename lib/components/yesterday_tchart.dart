import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/models/home_page_ydata.dart';

class YesterdayTChart extends StatelessWidget {
  final List<HomeYesterdayData> data;

  const YesterdayTChart({@required this.data});
  @override
  Widget build(BuildContext context) {
    final List<charts.Series<HomeYesterdayData, String>> series = [
      charts.Series(
        id: "Yesterdayt",
        data: data,
        domainFn: (HomeYesterdayData series, _) => series.timeString,
        measureFn: (HomeYesterdayData series, _) => series.temp,
        colorFn: (HomeYesterdayData series, _) => series.barColor,
        labelAccessorFn: (HomeYesterdayData series, _) => '${series.temp}°C',
      ),
    ];

    return Expanded(
      child: charts.BarChart(
        series,
        animate: true,
        barRendererDecorator: charts.BarLabelDecorator<String>(),
        animationDuration: const Duration(milliseconds: 1500),
      ),
    );
  }
}
