import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/models/home_page_middle_object.dart';

class YesterdayLChart extends StatelessWidget {
  final List<HomeLightYesterdayDataMiddle> data;

  const YesterdayLChart({@required this.data});
  @override
  Widget build(BuildContext context) {
    final List<charts.Series<HomeLightYesterdayDataMiddle, String>> series = [
      charts.Series(
        id: "Yesterdayl",
        data: data,
        domainFn: (HomeLightYesterdayDataMiddle series, _) => series.dayOrLight,
        measureFn: (HomeLightYesterdayDataMiddle series, _) => series.percent,
        colorFn: (HomeLightYesterdayDataMiddle series, _) => series.barColor,
        labelAccessorFn: (HomeLightYesterdayDataMiddle series, _) =>
            '${series.percent}%',
      ),
    ];

    return Expanded(
      child: charts.PieChart(
        series,
        animate: true,
        animationDuration: const Duration(milliseconds: 1500),
        behaviors: [
          charts.DatumLegend(
            position: charts.BehaviorPosition.end,
            outsideJustification: charts.OutsideJustification.endDrawArea,
            horizontalFirst: false,
            desiredMaxRows: 2,
            cellPadding: const EdgeInsets.only(right: 4.0, bottom: 4.0),
            entryTextStyle: const charts.TextStyleSpec(
                color: charts.MaterialPalette.black,
                fontFamily: 'Georgia',
                fontSize: 11,
            ),
          )
        ],
        defaultRenderer: charts.ArcRendererConfig(
          arcRendererDecorators: [
            charts.ArcLabelDecorator(
              labelPosition: charts.ArcLabelPosition.auto,
            ),
          ],
        ),
      ),
    );
  }
}
