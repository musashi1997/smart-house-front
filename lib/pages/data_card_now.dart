import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_course/config/constants.dart';
import 'package:flutter_course/config/data_manager.dart';
import 'package:flutter_course/models/home_page_now_data.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DataCardNow extends StatefulWidget {
  @override
  _DataCardNow createState() => _DataCardNow();
}

class _DataCardNow extends State<DataCardNow> {
  DateTime date;
  Future<HomeNowData> _myData = API.getNowRequest();
  @override
  Widget build(BuildContext context) {
    return Center(
        child: FutureBuilder<HomeNowData>(
      future: _myData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          date = DateTime.parse(snapshot.data.dateTime);
          date =
              date.add(const Duration(hours: 4, minutes: 30)); //to tehran time
          //Text('${snapshot.data.dateTime}')
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: SizedBox(
              height: 300,
              child: FractionallySizedBox(
                widthFactor: 0.92,
                child: Column(
                  children: [
                    const Padding(padding: EdgeInsets.all(20.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(25),
                          child: Column(
                            children: [
                              Text(
                                '${snapshot.data.temp / 10}' + '°C',
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 50,
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(
                                  top: 20.0,
                                ),
                              ),
                              Text(
                                'Humidity: ${'${snapshot.data.humidity}'}%',
                                style: const TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Constants.blueDark,
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(
                                  top: 20.0,
                                ),
                              ),
                              Text(
                                snapshot.data.light
                                    ? 'Your house is bright as Day!'
                                    : 'Your house is a bit dark!',
                                style: const TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.only(
                                  top: 43.0,
                                ),
                              ),
                              Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Fluttertoast.showToast(
                                          msg: 'fetching latest data',
                                      );
                                      setState(
                                        () {
                                          _myData = API.getNowRequest();
                                        },
                                      );
                                    },
                                    child: const Icon(
                                      Icons.refresh,
                                      color: Constants.blueDark,
                                      size: 18.0,
                                    ),
                                  ),
                                  Text(
                                    ' Last update: ${date.toString().substring(11, 16)}',
                                    style: const TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                  const Padding(padding: EdgeInsets.only(right: 58))
                                ],
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            padding: const EdgeInsets.only(right: 20),
                            child: Image.asset(
                              snapshot.data.temp < 255
                                  ? "lib/assets/temp_low.png"
                                  : "lib/assets/temp_high.png",
                              width: 75.0,
                              height: 175.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        // By default, show a loading spinner.
        return const CircularProgressIndicator();
      },
    ),
    );
  }
}
