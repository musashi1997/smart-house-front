import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_course/components/yesterday_hchart.dart';
import 'package:flutter_course/config/data_manager.dart';
import 'package:flutter_course/models/home_page_ydata.dart';

class DataCardYesterdayH extends StatefulWidget {
  @override
  _DataCardYesterdayH createState() => _DataCardYesterdayH();
}

class _DataCardYesterdayH extends State<DataCardYesterdayH> {
  final Future<List<HomeYesterdayData>> _myData = API.getYesterdayRequest();
  @override
  Widget build(BuildContext context) {
    return Center(
        child: FutureBuilder<List<HomeYesterdayData>>(
      future: _myData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          //Text('${snapshot.data.dateTime}')
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: SizedBox(
              height: 350,
              child: FractionallySizedBox(
                widthFactor: 0.92,
                //heightFactor: 0.4,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      const Text(
                        "Yesterday Humidity",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                      YesterdayHChart(
                        data: snapshot.data,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        // By default, show a loading spinner.
        return const CircularProgressIndicator();
      },
    ),
    );
  }
}
