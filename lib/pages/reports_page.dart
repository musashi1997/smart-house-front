import 'package:flutter/material.dart';
import 'package:flutter_course/pages/data_card_yesderdayh.dart';
import 'package:flutter_course/pages/data_card_yesterdayl.dart';
import 'package:flutter_course/pages/data_card_yesterdayt.dart';
import 'package:flutter_course/pages/drawer.dart';

class Reports extends StatelessWidget {
  final _myScaffoldKey = GlobalKey<ScaffoldState>();
//todo get from API
  //final HomeNowData nowData = API.getNowRequest();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _myScaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Reports'),
      ),
      drawer: MyDrawer(),
      body: ListView(
        children: [
          Center(
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 10,
                ),
                DataCardYesterday(),
                const SizedBox(
                  height: 10,
                ),
                DataCardYesterdayH(),
                const SizedBox(
                  height: 10,
                ),
                DataCardYesterdayL()
              ],
            ),
          )
        ],
      ),
    );
  }
}
