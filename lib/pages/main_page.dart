import 'package:flutter/material.dart';
import 'package:flutter_course/components/light_switches.dart';
import 'package:flutter_course/pages/data_card_now.dart';
import 'package:flutter_course/pages/data_card_yesderdayh.dart';
import 'package:flutter_course/pages/data_card_yesterdayl.dart';
import 'package:flutter_course/pages/data_card_yesterdayt.dart';
import 'package:flutter_course/pages/drawer.dart';

class MainPage extends StatelessWidget {
  final _myScaffoldKey = GlobalKey<ScaffoldState>();
//todo get from API
  //final HomeNowData nowData = API.getNowRequest();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _myScaffoldKey,
      appBar: AppBar(
        title: const Text('Home'),
        leading: IconButton(
          icon: const Icon(Icons.menu_rounded),
          onPressed: () {
            _myScaffoldKey.currentState.openDrawer();
          },
        ),
      ),
      drawer: MyDrawer(),
      body: ListView(
        children: [
          Center(
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 10,
                ),
                DataCardNow(),
                const SizedBox(
                  height: 10,
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: SizedBox(
                    child: FractionallySizedBox(
                      widthFactor: 0.92,
                      child: Column(
                        children: const [
                          // Padding(padding: const EdgeInsets.all(5.0)),
                          SwitchLightsButtons(),
                          // Padding(padding: const EdgeInsets.all(5.0)),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                DataCardYesterday(),
                const SizedBox(
                  height: 10,
                ),
                DataCardYesterdayH(),
                const SizedBox(
                  height: 10,
                ),
                DataCardYesterdayL()
              ],
            ),
          )
        ],
      ),
    );
  }
}
