import 'package:flutter/material.dart';
import 'package:flutter_course/components/user_data_extractor.dart';
import 'package:flutter_course/pages/drawer.dart';

class UserProfile extends StatelessWidget {
  final _myScaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _myScaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Profile'),
      ),
      drawer: MyDrawer(),
      body: ListView(
        children: [
          Center(
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 10,
                ),
                UserCard(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
