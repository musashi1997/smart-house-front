import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_course/config/constants.dart';
import 'package:flutter_course/config/endpoints.dart';
import 'package:flutter_course/models/user_login_data.dart';
import 'package:flutter_course/pages/main_page.dart';
import 'package:flutter_course/pages/sign_up.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPage createState() => _AuthPage();
}

class _AuthPage extends State<AuthPage> {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  DateTime currentBackPressTime;
  Future<bool> onWillPop() {
    final DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > const Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: 'tap again to exit');
      return Future.value(false);
    }
    exit(0);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Form(
            key: _formkey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                  child: Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 100.0),
                      width: 200,
                      height: 150,
                      // decoration: BoxDecoration(
                      //     color: Colors.black,
                      //     borderRadius: BorderRadius.circular(50.0)),
                      child: Image.asset('lib/assets/logo2.png'),
                    ),
                  ),
                ),
                Padding(
                  //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                  padding: const EdgeInsets.only(
                    left: 15,
                    right: 45,
                    top: 15,
                  ),
                  child: TextFormField(
                    controller: _username,
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 2,
                          color: Constants.blueLight,
                        ),
                      ),
                      icon: Icon(
                        Icons.account_box,
                      ),
                      border: OutlineInputBorder(),
                      labelText: 'Username',
                      hintText: 'Enter your username',
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return "Please enter username";
                      }
                      return null;
                    },
                    onSaved: (String email) {},
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    right: 45.0,
                    top: 15,
                    bottom: 15,
                  ),
                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    controller: _password,
                    obscureText: true,
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 2,
                          color: Constants.blueLight,
                        ),
                      ),
                      icon: Icon(Icons.password),
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Enter secure password',
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return "Please enter password";
                      }
                      return null;
                    },
                  ),
                ),

                Container(
                  height: 50,
                  width: 250,
                  margin: const EdgeInsets.only(top: 40),
                  decoration: BoxDecoration(
                    color: Constants.blueDark,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: TextButton(
                    onPressed: () {
                      if (_formkey.currentState.validate()) {
                        loginUser();
                      }
                    },
                    child: const Text(
                      'Login',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (_) => FormPage()),
                    );
                  },
                  child: const Text(
                    'New User? Create Account',
                    style: TextStyle(color: Constants.blueDark, fontSize: 15),
                  ),
                ),
                // Text('New User? Create Account')
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future loginUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final Map userdata = {'username': _username.text, 'Pass': _password.text};
    try {
      final response = await http.post(Uri.parse(UsersEndpoints.authenticate),
          headers: {'Content-Type': 'application/json; charset=UTF-8'},
          body: jsonEncode(userdata));

      if (response.statusCode == 200) {
        final Map<String, dynamic> decoded =
            json.decode(response.body) as Map<String, dynamic>;
        final UserData userData = UserData.fromJson(decoded);

        prefs.setInt('userId', userData.userId);
        prefs.setString('username', userData.username);
        prefs.setString('email', userData.email);
        prefs.setString('token', userData.token);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => MainPage(),
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: const Duration(seconds: 2),
            backgroundColor: Colors.grey[900],
            content: const Text(
              "Successfully logged in",
              style: TextStyle(color: Colors.green),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: const Duration(seconds: 2),
            backgroundColor: Colors.grey[900],
            content: const Text(
              'wrong user or pass',
              style: TextStyle(color: Colors.red),
            ),
          ),
        );
        Navigator.push(context, MaterialPageRoute(builder: (_) => AuthPage()));
      }
    } catch (e) {}
  }
}
