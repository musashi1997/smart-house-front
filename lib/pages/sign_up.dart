import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_course/config/constants.dart';
import 'package:flutter_course/config/endpoints.dart';
import 'package:http/http.dart' as http;

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final TextEditingController _name = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirmpassword = TextEditingController();
  final TextEditingController _deviceSerialNum = TextEditingController();

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  final String _response = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formkey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    width: 200,
                    height: 200,
                    // decoration: BoxDecoration(
                    //     color: Colors.black,
                    //     borderRadius: BorderRadius.circular(50.0)),
                    child: Image.asset('lib/assets/logo3.png'),
                  ),
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                  bottom: 15,
                ),
                child: TextFormField(
                  controller: _name,
                  decoration: const InputDecoration(
                    icon: Icon(
                      Icons.person,
                    ),
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                    hintText: 'Enter username ',
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "Please enter name";
                    }
                    return null;
                  },
                  onSaved: (String name) {},
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _email,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.email),
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email as abc@gmail.com',
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "Please enter  email";
                    }
                    if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                        .hasMatch(value)) {
                      return "Please enter valid email";
                    }
                    return null;
                  },
                  onSaved: (String email) {},
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                ),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _password,
                  obscureText: true,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.lock),
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password',
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "Please enter password";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 15.0,
                  right: 15.0,
                  top: 15,
                ),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  controller: _confirmpassword,
                  obscureText: true,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.verified_user),
                    border: OutlineInputBorder(),
                    labelText: 'Confirm Password',
                    hintText: 'Confirm',
                  ),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "Please enter re-password";
                    }
                    if (_password.text != _confirmpassword.text) {
                      return "Password Do not match";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  controller: _deviceSerialNum,
                  decoration: InputDecoration(
                      icon: Icon(Icons.code),
                      border: OutlineInputBorder(),
                      labelText: 'Device Code',
                      hintText: 'Enter Provided Device code'),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return "Please enter Device Code";
                    }
                    return null;
                  },
                  onSaved: (String email) {},
                ),
              ),
              Text(
                _response,
                style: TextStyle(color: Colors.red[800]),
              ),
              Container(
                height: 50,
                width: 250,
                margin: const EdgeInsets.only(top: 40),
                decoration: BoxDecoration(
                  color: Constants.blueDark,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: TextButton(
                  onPressed: () {
                    if (_formkey.currentState.validate()) {
                      registrationUser();
                    }
                  },
                  child: const Text(
                    'Sign Up',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),

              // Text('New User? Create Account')
            ],
          ),
        ),
      ),
    );
  }

  Future registrationUser() async {
    // url to registration php script
    //json maping user entered details
    final Map mapeddate = {
      'Username': _name.text,
      'Email': _email.text,
      'Pass': _password.text,
      'deviceId': _deviceSerialNum.text,
    };
    try {
      final response = await http.post(
        Uri.parse(UsersEndpoints.register),
        headers: {'Content-Type': 'application/json; charset=UTF-8'},
        body: jsonEncode(mapeddate),
      );
      if (response.statusCode != 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: const Duration(seconds: 2),
            backgroundColor: Colors.grey[900],
            content: Text(
              response.body,
              style: const TextStyle(color: Colors.red),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: const Duration(seconds: 2),
            backgroundColor: Colors.grey[900],
            content: Text(
              '${response.body} login please',
              style: const TextStyle(color: Colors.green),
            ),
          ),
        );
        Navigator.of(context).pop();
      }
      //getting response from php code, here
      // var data = jsonDecode(reponse.body);

    } catch (ex) {}
  }
}
