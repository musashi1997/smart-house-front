import 'package:flutter/material.dart';
import 'package:flutter_course/pages/drawer.dart';

class AboutUs extends StatelessWidget {
  final _aboutscaffKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _aboutscaffKey,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('About Us'),
      ),
      drawer: MyDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.white,
                    child: Image.asset('lib/assets/KNTU.png'),
                  ),
                  CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.white,
                    child: Image.asset('lib/assets/nerg.png'),
                  ),
                ],
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: SizedBox(
                child: FractionallySizedBox(
                  widthFactor: 0.92,
                  //heightFactor: 0.4,
                  child: Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.all(20),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          textBaseline: TextBaseline.alphabetic,
                          textDirection: TextDirection.ltr,
                          children: const <Widget>[
                            Align(
                              child: Text(
                                'Smart House project',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'creator and developer: amirmohammad abedini\nemail: amirmohammad.abedini@gmail.com\n ',
                                style: TextStyle(
                                  // fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
