import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_course/components/yesterday_lchart.dart';
import 'package:flutter_course/config/data_manager.dart';
import 'package:flutter_course/models/home_page_middle_object.dart';

class DataCardYesterdayL extends StatefulWidget {
  @override
  _DataCardYesterdayL createState() => _DataCardYesterdayL();
}

class _DataCardYesterdayL extends State<DataCardYesterdayL> {
  final Future<List<HomeLightYesterdayDataMiddle>> _myData =
      API.getYesterdayLightRequest();
  @override
  Widget build(BuildContext context) {
    return Center(
        child: FutureBuilder<List<HomeLightYesterdayDataMiddle>>(
      future: _myData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: SizedBox(
              height: 350,
              child: FractionallySizedBox(
                widthFactor: 0.92,
                //heightFactor: 0.4,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      const Text(
                        "Yesterday House Brighteness",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                      YesterdayLChart(
                        data: snapshot.data,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        // By default, show a loading spinner.
        return const CircularProgressIndicator();
      },
    ),
    );
  }
}
