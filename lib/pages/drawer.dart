import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/config/constants.dart';
import 'package:flutter_course/config/data_manager.dart';
import 'package:flutter_course/pages/main_page.dart';
import 'package:flutter_course/pages/reports_page.dart';
import 'package:flutter_course/pages/user_profile.dart';

import './about_us.dart';
import 'authentication.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text('Menu'),
          ),
          ListTile(
            title: const Text(
              'Home',
              style: TextStyle(color: Colors.white),
            ),
            leading: const Icon(
              Icons.home,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => MainPage()),
              );
            },
          ),
          ListTile(
            title: const Text(
              'Reports',
              style: TextStyle(color: Colors.white),
            ),
            leading: const Icon(
              Icons.business,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => Reports()),
              );
            },
          ),
          ListTile(
            title: const Text(
              'Profile',
              style: TextStyle(color: Colors.white),
            ),
            leading: const Icon(
              Icons.account_circle_outlined,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => UserProfile()),
              );
            },
          ),
          ListTile(
            title: const Text(
              'About Us',
              style: TextStyle(color: Colors.white),
            ),
            leading: const Icon(
              Icons.info_outline,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => AboutUs()),
              );
            },
          ),
          ListTile(
            title: Text(
              'Log out',
              style: TextStyle(
                fontWeight: FontWeight.w800,
                foreground: Paint()..color = Constants.redDark,
              ),
            ),
            leading: const Icon(
              Icons.logout,
              color: Colors.white,
            ),
            onTap: () {
              API.logOut();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => AuthPage(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
