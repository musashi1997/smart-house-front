import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class HomeLightYesterdayData {
  final double wasOn;
  final double wasOff;
  final charts.Color barColor1;
  final charts.Color barColor2;
  HomeLightYesterdayData({
    @required this.wasOn,
    @required this.wasOff,
    @required this.barColor1,
    @required this.barColor2,
  });
  factory HomeLightYesterdayData.fromJson(Map<String, dynamic> json) {
    return HomeLightYesterdayData(
      wasOn: json['wasOn'] * 100 as double,
      wasOff: json['wasOff'] * 100 as double,
      barColor1: charts.ColorUtil.fromDartColor(Colors.blue[300]),
      barColor2: charts.ColorUtil.fromDartColor(Colors.blue[900]),
    );
  }

  Map<String, dynamic> toJson() => {
        'wasOn': wasOn,
        'wasOff': wasOff,
      };
}
