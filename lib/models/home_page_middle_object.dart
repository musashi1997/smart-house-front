import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';

class HomeLightYesterdayDataMiddle {
  final double percent;
  final String dayOrLight;
  final charts.Color barColor;

  HomeLightYesterdayDataMiddle({
    @required this.percent,
    @required this.dayOrLight,
    @required this.barColor,
  });
}
