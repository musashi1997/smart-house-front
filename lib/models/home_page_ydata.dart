import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class HomeYesterdayData {
  final double temp;
  final int humidity;
  final String timeString;
  final charts.Color barColor;

  HomeYesterdayData(
      {@required this.temp,
      @required this.timeString,
      @required this.humidity,
      @required this.barColor,
  });

  factory HomeYesterdayData.fromJson(Map<String, dynamic> json) {
    charts.Color barColor;
    switch (json['timeOfDay'] as String) {
      case 'morning':
        barColor = charts.ColorUtil.fromDartColor(Colors.blue[200]);
        break;
      case 'noon':
        barColor = charts.ColorUtil.fromDartColor(Colors.blue[500]);
        break;
      case 'evening':
        barColor = charts.ColorUtil.fromDartColor(Colors.orange[700]);
        break;
      case 'night':
        barColor = charts.ColorUtil.fromDartColor(Colors.blue[900]);
        break;
    }
    return HomeYesterdayData(
      temp: json['temp'] / 10 as double,
      humidity: json['humidity'] as int,
      timeString: json['timeOfDay'] as String,
      barColor: barColor,
    );
  }

  Map<String, dynamic> toJson() => {
        'userId': temp,
        'username': humidity,
        'email': timeString,
      };
}
