import 'package:flutter/foundation.dart';

class LightsState {
  final bool l1state;
  final bool l2state;
  final bool l3state;
  LightsState(
      {@required this.l1state, @required this.l2state, @required this.l3state});

  factory LightsState.fromJson(Map<String, dynamic> json) {
    return LightsState(
        l1state: json['l1state'] as bool,
        l2state: json['l2state'] as bool,
        l3state: json['l3state'] as bool);
  }
  Map<String, dynamic> toJson() =>
      {'l1state': l1state, 'l2state': l2state, 'l3state': l3state};
}
