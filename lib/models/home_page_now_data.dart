import 'package:flutter/foundation.dart';

class HomeNowData {
  final int temp;
  final int humidity;
  final bool light;
  final String dateTime;
  HomeNowData(
      {@required this.temp,
      @required this.humidity,
      @required this.light,
      @required this.dateTime,
  });

  factory HomeNowData.fromJson(Map<String, dynamic> json) {
    return HomeNowData(
      temp: json['temp'] as int,
      humidity: json['humidity'] as int,
      light: json['light'] as bool,
      dateTime: json['dateTime'] as String,
    );
  }
  Map<String, dynamic> toJson() => {
        'temp': temp,
        'humidity': humidity,
        'light': light,
        'dateTime': dateTime,
      };
}
