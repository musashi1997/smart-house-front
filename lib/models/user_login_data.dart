class UserData {
  final int userId;
  final String username;
  final String email;
  final String token;

  UserData({this.userId, this.username, this.email, this.token});

  factory UserData.fromJson(Map<String, dynamic> json) {
    return UserData(
      userId: json['userId'] as int,
      username: json['username'] as String,
      email: json['email'] as String,
      token: json['token'] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        'userId': userId,
        'username': username,
        'email': email,
        'token': token,
      };
}
