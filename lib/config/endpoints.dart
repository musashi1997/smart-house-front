const String baseURL = 'http://musashiro.ir:6001';

class LightEndpoints {
  static String lights = '$baseURL/lights';
  static String getLights({int id}) {
    return '$lights${'/$id'}';
  }
  static String getDeviceLight({String guid}) {
    return '$lights${'/device/$guid'}';
  }
}

class LogsEndpoints {
  static String logs = '$baseURL/device/logs';
  static String logsById({String id}) {
    return '$logs${'/$id'}';
  }
  static String logsNow = '$logs/now';
  static String logsYesterday = '$logs/yesterday';
  static String logsYesterdayBrightness = '$logsYesterday/brightness';
  static String logsToday({String id}){
    return '$logs${'/today/$id'}';
  }
  static String logsToMonth({String id}){
    return '$logs${'/tomonth/$id'}';
  }
  static String logsToYear({String id}){
    return '$logs${'/toyear/$id'}';
  }
}

class UsersEndpoints {
  static String users = '$baseURL/Users';
  static String usersById({String id}){
    return '$users${'/$id'}';
  }
  static String authenticate = '$users/authenticate';
  static String register = '$users/register';
  static String email({String email}){
    return '$users${'/email/$email'}';
  }
}
