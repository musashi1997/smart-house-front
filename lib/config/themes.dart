import 'package:flutter/material.dart';

import 'constants.dart';

class AppThemes {
  static ThemeData light = ThemeData.light().copyWith(
    brightness: Brightness.light,
    hoverColor: Colors.white.withOpacity(0.15),
    scaffoldBackgroundColor: Constants.whiteDark,
    primaryColor: Constants.blueDark,
    canvasColor: Constants.blueDark,
    highlightColor: Constants.blueLight,
    cardColor: Constants.greyLight,
    appBarTheme: AppBarTheme(
      color: Constants.blueDark,
    ),
  );
}
