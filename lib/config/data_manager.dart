import 'dart:async';
import 'dart:convert';

import 'package:flutter_course/config/endpoints.dart';
import 'package:flutter_course/models/home_page_middle_object.dart';
import 'package:flutter_course/models/home_page_now_data.dart';
import 'package:flutter_course/models/home_page_ydata.dart';
import 'package:flutter_course/models/home_page_ydata_light.dart';
import 'package:flutter_course/models/light_state.dart';
import 'package:flutter_course/models/user_login_data.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class API {
  static Future<UserData> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final UserData user = UserData(
      userId: prefs.getInt('userId'),
      username: prefs.getString('username'),
      email: prefs.getString('email'),
    );
    return user;
  }

  static Future<LightsState> getLightsState() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int userId = prefs.getInt('userId');
    final String token = prefs.getString('token');
    final response = await http.get(
      Uri.parse(LightEndpoints.getLights(id: userId)),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'bearer $token',
      },
    );
    if (response.statusCode == 200) {
      final List<dynamic> decoded = json.decode(response.body) as List<dynamic>;
      final LightsState data = LightsState.fromJson(decoded[0] as Map<String, dynamic>);
      return data;
    } else {
      throw Exception('Failed to get light states');
    }
  }

  static Future logOut() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userId');
    prefs.remove('username');
    prefs.remove('token');
    prefs.remove('email');
  }

  static Future<List<HomeLightYesterdayDataMiddle>> getYesterdayLightRequest() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');
    final response = await http.get(
      Uri.parse(LogsEndpoints.logsYesterdayBrightness),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> decoded = json.decode(response.body) as Map<String, dynamic>;
      final HomeLightYesterdayData yesLData =
          HomeLightYesterdayData.fromJson(decoded);
      final List<HomeLightYesterdayDataMiddle> finished = [
        HomeLightYesterdayDataMiddle(
          percent: yesLData.wasOn,
          dayOrLight: 'lights On',
          barColor: yesLData.barColor1,
        ),
        HomeLightYesterdayDataMiddle(
          percent: yesLData.wasOff,
          dayOrLight: 'lights Off',
          barColor: yesLData.barColor2,
        )
      ];
      return finished;
    } else {
      throw Exception('Failed to get yesterday brightness');
    }
  }

  static Future<HomeNowData> getNowRequest() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');
    final response = await http.get(Uri.parse(LogsEndpoints.logsNow),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'bearer $token',
        },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> decoded = json.decode(response.body) as Map<String, dynamic>;
      final HomeNowData nowData = HomeNowData.fromJson(decoded);
      return nowData;
    } else {
      throw Exception('Failed to get now logs');
    }
  }
  static Future<http.Response> yesterdayAPI({String type, String token}){
    final Map<String, String> queryParams = {
      'query': type
    };
    return http.get(
        Uri.parse('${LogsEndpoints.logsYesterday}?${Uri(queryParameters: queryParams).query}'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'bearer $token',
        },
    );
  }


  static Future<List<HomeYesterdayData>> getYesterdayRequest() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');
    final response1 = await yesterdayAPI(token: token, type: 'morning');
    final response2 = await yesterdayAPI(token: token, type: 'noon');
    final response3 = await yesterdayAPI(token: token, type: 'evening');
    final response4 = await yesterdayAPI(token: token, type: 'night');


    if (response1.statusCode == 200 &&
        response2.statusCode == 200 &&
        response3.statusCode == 200 &&
        response4.statusCode == 200) {
      final Map<String, dynamic> decoded1 = json.decode(response1.body) as Map<String, dynamic>;
      final Map<String, dynamic> decoded2 = json.decode(response2.body) as Map<String, dynamic>;
      final Map<String, dynamic> decoded3 = json.decode(response3.body) as Map<String, dynamic>;
      final Map<String, dynamic> decoded4 = json.decode(response4.body) as Map<String, dynamic>;
      final HomeYesterdayData yesterday1 = HomeYesterdayData.fromJson(decoded1);
      final HomeYesterdayData yesterday2 = HomeYesterdayData.fromJson(decoded2);
      final HomeYesterdayData yesterday3 = HomeYesterdayData.fromJson(decoded3);
      final HomeYesterdayData yesterday4 = HomeYesterdayData.fromJson(decoded4);

      final List<HomeYesterdayData> yestArr = <HomeYesterdayData>[];
      yestArr.add(yesterday1);
      yestArr.add(yesterday2);
      yestArr.add(yesterday3);
      yestArr.add(yesterday4);
      return yestArr;
    } else {
      throw Exception('Failed to get yesterday requests');
    }
  }
}
