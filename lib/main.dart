import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'pages/authentication.dart';
import 'pages/main_page.dart';
import 'package:flutter_course/config/themes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var token = prefs.getString('token');
  runApp(
    MaterialApp(
      theme: AppThemes.light,
      themeMode: ThemeMode.light,
      home: token == null ? AuthPage() : MainPage(),
    ),
  );
}
